# Ready to Use ArchLinux for retrogaming-ps2, ... 



archlinux with the PS2 emulator, preinstalled with pulseaudio and HDMI audio support.
Preinstalled with a newer Linux kernel, which offers an excellent hardware support.

# Download

ROOTFS:

https://gitlab.com/openbsd98324/retrogaming-ps2/-/raw/main/v1/retrogaming-ps2-archlinux-amd64-v1.tar.gz


# Installation 

The project nanogrub can be used.

zcat image-nanogrub-boot-efi-sdb2-abook-devuan-archlinux-v1.mbr.gz  > /dev/sdX 

Create for instance /dev/sdb2 with ext3, mkfs.ext3, and copy the content of retrogaming-ps2-archlinux-amd64-v1.tar.gz.

cd /media/sdb2

tar xvpfz retrogaming-ps2-archlinux-amd64-v1.tar.gz


# Bios 

The tarball contains opensource software, with source code. Archlinux, Opensource Project with Linux kernel.

No copyrighted is provided in the archive. Use your own PS2 and create your own scp SCPH-10000 or accordingly to the hardware.


# Media

![](media/image/emulator.png)


![](media/image/example.png)

![](media/image/shadow-config.png)

![](media/image/gamepad.png)



For more games?

Example: 

 pacman -S --noconfirm  sauerbraten
